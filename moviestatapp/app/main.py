#!/usr/bin/python3

from flask import abort, Flask, jsonify, render_template, request
import psycopg2
import psycopg2.extras
import os
import re


app = Flask(__name__)

db_settings = {
        "user": os.environ['POSTGRES_USER'],
        "password": os.environ['POSTGRES_PASSWORD'],
        "dbname": os.environ['POSTGRES_DB'],
        "host": os.environ['POSTGRES_ADDRESS']
        }
only_int = re.compile('^\d+$')


@app.route('/')
@app.route('/allmovieslist')
def allmovieslist():
    ''' show page with all movies in database
    '''
    listname = "All movies list"

    try:
        connection = psycopg2.connect(**db_settings)
        cur = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("""SELECT * FROM movies;""")
        movies = cur.fetchall()
    except:
        abort(500)
    finally: 
        connection.commit()
    
    return render_template("moviespage.html",
            listname=listname,
            movies=movies
            )


@app.route('/stat')
def statistics():
    ''' show page with all movies statistics
    '''
    listname = "Movies statistics"
    try:
        connection = psycopg2.connect(**db_settings)
        cur = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("""SELECT * FROM statistic;""")
        statistics = cur.fetchall()
    except:
        abort(500)
    finally:
        connection.commit()

    return render_template("statpage.html",
            listname=listname,
            statistics=statistics
            )


@app.route('/movies', methods=['GET'])
def getmovies():
    ''' return all movies in json
    '''
    try:
        connection = psycopg2.connect(**db_settings)
        cur = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("""SELECT * FROM movies;""")
        
        movies = []
        for item in cur.fetchall():
            themovie = {}
            themovie['name'] = item['name']
            themovie['id'] = item['id']
            themovie['description'] = item['description']
            themovie['actors'] = item['actors']
            movies.append(themovie)
    except:
        abort(500)
    finally:
        connection.commit()

    return jsonify({'result': movies})


@app.route('/putstat', methods=['POST'])
def putstat():
    ''' recieve the movie view statistics by the customer
        and insert it in database
    '''
    if not request.json or \
       not 'customer' in request.json or \
       not 'movies_id' in request.json or \
       not 'viewtime' in request.json or \
       not only_int.match(str(request.json['customer'])) or \
       not only_int.match(str(request.json['movies_id'])) or \
       not only_int.match(str(request.json['viewtime'])):
        abort(400)

    try:
        connection = psycopg2.connect(**db_settings)
        cur = connection.cursor()
        query = """INSERT INTO statistic(customer, movies_id, viewtime)\
                VALUES ('{0}', '{1}', '{2}');"""\
                .format(request.json.get("customer"),
                        request.json.get("movies_id"),
                        request.json.get("viewtime"))
        cur.execute(query)
    except :
        abort(500)
    finally:
        connection.commit()

    return "Statistics add to database\n"
