#!/usr/bin/python3

import re
from bs4 import BeautifulSoup
import urllib.request
import time
import json
import random


link_prefix = "http://kinozal.tv"
link_top = "http://kinozal.tv/top.php?t=1&d=0&k=0&f=0&w=0&s=0"
proxies = ['http://37.193.66.39:53281',
           'http://149.255.154.4:8080',
           'http://198.50.240.19:8080',
           'http://66.70.191.5:3128',
           'http://82.146.37.33:8888',
           'http://94.190.19.11:53281',
           'http://89.248.233.194:8080',
           'http://144.217.49.109:80',
           'http://149.56.40.69:80',
           'http://185.101.238.5:8080',
           'http://138.197.137.90:8080',
           'http://52.178.197.34:3128',
           'http://151.80.152.121:8080',
           'http://195.154.77.130:3128',
           'http://192.95.21.109:80'
        ]

while True:
    try:
        target_url = link_top
        print(target_url)
        page_request = urllib.request.Request(
                target_url,
                data=None,
                headers={
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36'
                    }
                )
        proxy = random.choice(proxies)
        proxy_support = urllib.request.ProxyHandler({'http' : proxy})
        opener = urllib.request.build_opener(proxy_support)
        urllib.request.install_opener(opener)

        top_parse = BeautifulSoup(urllib.request.urlopen(page_request), "html.parser")
        films = top_parse.find_all('a', href=re.compile('^/details.php'))
        break
    except Exception as e:
        print(str(e))

i = 1
for film in films:
    target_url = link_prefix + film['href']
    print(target_url)
    page_request = urllib.request.Request(
            target_url,
            data=None,
            headers={
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36'
                }
            )
    while True:
        try:
            proxy = random.choice(proxies)
            proxy_support = urllib.request.ProxyHandler({'http' : proxy})
            opener = urllib.request.build_opener(proxy_support)
            urllib.request.install_opener(opener)

            film_parse = BeautifulSoup(urllib.request.urlopen(page_request), "html.parser")
            film_title = film_parse.find('a', href=re.compile('^/details.php'))
            film_info = film_parse.find_all('div', {'class': 'bx1 justify'})
            file_name = 'films/film{0}.txt'.format(i)
            film_file = open(file_name, 'w')
            print(film_title, file=film_file)
            for item in film_info:
                print(item, file=film_file)
            film_file.close()
            i += 1
            break
        except Exception as e:
            print(str(e))
            time.sleep(random.randint(5, 10))
    time.sleep(random.randint(5, 10))
