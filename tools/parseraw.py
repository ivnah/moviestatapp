#!/usr/bin/python3

import re
from bs4 import BeautifulSoup
import os
import json


raw_dir = 'raw/'
json_dir = 'json/'
for filenames in os.walk(raw_dir):
    for filename in filenames[2]:

        jsonfilename = filename.split('.')[0] + '.json'
        raw_film_info_file = open(os.path.join(raw_dir, filename), 'r')
        json_film_info_file = open(os.path.join(json_dir, jsonfilename), 'w')

        raw_parser = BeautifulSoup(raw_film_info_file.read(), 'html.parser')
        title = raw_parser.find('a')['title'].split('/')[0].split('(')[0]
        desc = raw_parser.find('p').text[10:]
        cast = raw_parser.find('b', text="В ролях:")

        film_info_json = {}
        film_info_json['name'] = title
        film_info_json['description'] = desc
        film_info_json['actors'] = []
        for actor in cast.find_next_siblings('a'):
            film_info_json['actors'].append(actor.text)

        json.dump(film_info_json, json_film_info_file)

        raw_film_info_file.close()
        json_film_info_file.close()
