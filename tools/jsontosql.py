#!/usr/bin/python3

'''
insert movies info from json files to database
'''

import psycopg2
import os
import json

films_info_json_dir = 'json/'
db_settings = {
        "host": "172.17.0.5",
        "dbname": "moviestatapp",
        "user": "dbadm",
        "password": "somepass"
        }

connection = psycopg2.connect(**db_settings)

for files_list in os.walk(films_info_json_dir):
    for filename in files_list[2]:

        file_path = os.path.join(films_info_json_dir, filename)
        film_info_json_file = open(file_path, 'r')
        film_info = json.load(film_info_json_file)

        cur = connection.cursor()
        query = """INSERT INTO movies(name, description, actors) VALUES ('{0}', '{1}', '{{{2}}}');"""\
                .format(film_info['name'],
                        film_info['description'],
                        str(film_info['actors'])[1:-1].replace('\'', '\"'))
        cur.execute(query)

        connection.commit()
        film_info_json_file.close()
