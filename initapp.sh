#!/bin/bash

# script for build movies statistics service

DB_CONTAINER_NAME="dbserv"
APP_CONTAINER_NAME="moviestatapp"
BALANCER_CONTAINER_NAME="balancer"

POSTGRESUSER="dbadm"
POSTGRESPASS="somepass"
POSTGRESDB="moviestatapp"

# check recieved quantity of instances
if [ -z "$1" ];
    then instances=1;
    else instances="$1";
fi
# check recieved memory limit
if [ -z "$2" ];
    then memory="256m";
    else memory="$2m";
fi
# check recieved swap limit
if [ -z "$3" ];
    then swap="500m";
    else swap="$3m";
fi
# check recieved CPU's limit
if [ -z "$4" ];
    then cpu="0.3";
    else cpu="$4";
fi

# build images
echo 'Build images...'
docker pull debian:latest
docker pull postgres:latest
docker build dbserver -t dbserver
docker build nginx -t nginx:debian
docker build balancer -t balancer:nginx
docker build flask -t flask:nginx
docker build moviestatapp -t moviestatapp

# run database container
echo 'Run database container...'
docker run \
    --memory=$memory \
    --memory-swap=$swap \
    --cpus=$cpu \
    --name $DB_CONTAINER_NAME \
    --env POSTGRES_USER=$POSTGRESUSER \
    --env POSTGRES_PASSWORD=$POSTGRESPASS \
    --env POSTGRES_DB=$POSTGRESDB \
    -d dbserver

DB_CONTAINER_ADDRESS=$(docker inspect -f \
    '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
    $DB_CONTAINER_NAME)

#run requested app containers
declare -a APP_CONTAINER_ADDRESSES
for ((i=1;i<$instances+1;i++))
do
     echo " Run $i app container..."
     docker run \
         --memory=$memory \
         --memory-swap=$swap \
         --cpus=$cpu \
         --name $APP_CONTAINER_NAME$i \
         --env POSTGRES_USER=$POSTGRESUSER \
         --env POSTGRES_PASSWORD=$POSTGRESPASS \
         --env POSTGRES_DB=$POSTGRESDB \
         --env POSTGRES_ADDRESS=$DB_CONTAINER_ADDRESS \
         -d moviestatapp
     APP_CONTAINER_ADDRESSES+=($(docker inspect -f \
        '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
        $APP_CONTAINER_NAME$i))
done

#run balancer
docker run -d \
    --memory=$memory \
    --memory-swap=$swap \
    --cpus=$cpu \
    --name $BALANCER_CONTAINER_NAME balancer:nginx

BALANCER_CONTAINER_ADDRESS=$(docker inspect -f \
    '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
    $BALANCER_CONTAINER_NAME)

#make balancer's nginx.conf
echo "Config balancer..."
for i in "${APP_CONTAINER_ADDRESSES[@]}"
do
    str="$str\tserver $i;\n"
done
sed "/upstream/a \ $str" \
    balancer/moviestatapp.conf.default > balancer/moviestatapp.conf

docker cp balancer/moviestatapp.conf \
    $BALANCER_CONTAINER_NAME:/etc/nginx/sites-enabled/

docker exec $BALANCER_CONTAINER_NAME /etc/init.d/nginx reload

echo "!!!Make queries to balancer=$BALANCER_CONTAINER_ADDRESS!!!"
